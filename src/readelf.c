/////////////////////////////////////////////////////////////////////////////////
//           University of Hawaii, College of Engineering
/// @brief   Lab05a - readelf - EE 491 - Spr 2022
///
/// @file    readelf.c
/// @version 1.0 
///
/// readelf - Scans ELF file's ELF header and prints header information 
///
/// @author Jake Imanaka   <jimanaka@hawaii.edu>
/// @date   20_03_2022
///
///////////////////////////////////////////////////////////////////////////////

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <stdint.h>
#include <stdbool.h>
#include <string.h>

#include "readelf.h"
#include "scan-header.h"

// print usage and error out
// @Todo: add more usage otions as more are implemented
void error_print_help() {
   fprintf(stderr, "Usage: readelf <option(s)> elf-file(s)\n"
                   " Displays information about the contents of ELF format files\n"
                   " Options are:\n"
                   "  -h --file-header        Display the ELF file header\n");
   exit(EXIT_FAILURE);
}

// takes in filename and options from command line then scans file's ELF header and prints information
// @Todo implement more flags as needed
int main(int argc, char* argv[]) {

   FILE* fp = NULL;
   char* filename;
   bool h_flag = false;             // h flag
   struct Header header;            // stores header info
   int opt;                         // used to loop over CLI options
   bool multi_file_flag = false;    // if handling multiple files

   // loop over all command-line options
   // @Todo add support for long options (getoptlong?)
   while((opt = getopt(argc, argv, "h-:")) != -1)
   {
      switch (opt)
      {
         // if -h set h flag
         case 'h':
            h_flag = true;
            break;
         // if a long options is give (form of --[optarg])
         case '-':
            // check for file-header optarg
            if (strcmp(optarg, "file-header") == 0) {
               h_flag = true;
            } else {
               // error if optarg not recognized
               fprintf(stderr, "%s: unrecognized option '%s'\n", PROGRAM, optarg);
               error_print_help();
            }
            break;
         // handle unknown options
         case '?':
            error_print_help(); 
            break;
         // handle no string option
         // not used if this program only does -h
         case ':':
            fprintf(stderr, "option needs a value\n");
            exit(EXIT_FAILURE);
            break;
      }
   }

   // error if no CL option
   // @Todo generalize this error check for any flag
   if (h_flag == false) {
      error_print_help();
   }

   // check for filenames
   if(optind >= argc) {
      // error if no file name given
      fprintf(stderr, "%s: No filename provided\n", PROGRAM);
      exit(EXIT_FAILURE);
   }
   
   // check for more than one filename
   if(argc - optind > 1) {
      // set multi file flag if there are more than 1 filename
      multi_file_flag = true;
   }

   // loop to handle multiple files
   while(optind < argc) {
      filename = argv[optind];
      // opening file with error checking
      if((fp = fopen(filename, "r")) == NULL) {
         fprintf(stderr, "%s: Error: \'%s\': No such file\n", PROGRAM, filename);
         exit(EXIT_FAILURE);
      }

      // if multiple files, print filename first
      if(multi_file_flag) {
         printf("File: %s\n", filename);
      }
      // if h_flag is set, do scan and print of header
      if (h_flag) {
         // do scan and print of header
         scan_header(fp, &header);
         print_header(header);
      }
      printf("\n");

      // close file pointer and increment to next filename
      fclose(fp);
      optind++;
   }

   return 0;
}
