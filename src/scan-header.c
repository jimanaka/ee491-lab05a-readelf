/////////////////////////////////////////////////////////////////////////////////
//           University of Hawaii, College of Engineering
/// @brief   Lab05a - readelf - EE 491 - Spr 2022
///
/// @file    scan-header.c
/// @version 1.0 
///
/// readelf - Scans ELF file's ELF header and prints header information 
///
/// @author Jake Imanaka   <jimanaka@hawaii.edu>
/// @date   20_03_2022
///
///////////////////////////////////////////////////////////////////////////////

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>

#include "readelf.h"
#include "scan-header.h"

// supported types
enum e_types {
   ET_NONE = 0,
   ET_REL,
   ET_EXEC,
   ET_DYN,
   ET_CORE,
   ET_UK = 101,            // used for unknown, invalid, or not implemented options
   ET_LOPROC = 0xff00,
   ET_HIPROC = 0xffff
};
   
//https://www.linkedin.com/pulse/mapping-enum-string-c-language-sathishkumar-duraisamy
// linking type enum to print string
const char * const types[] = {
   [ET_NONE] = "NONE (No file type)",
   [ET_REL] = "REL (Relocatable file)",
   [ET_EXEC] = "EXEC (Executable file)",
   [ET_DYN] = "DYN (Position-Independent Executable file)",
   [ET_CORE] = "CORE (Core file)",
   [ET_UK] = "<unknown>",
   [ET_LOPROC] = "LOPROC (Processor-specific)",
   [ET_HIPROC] = "HIPROC (Processor-specific)"
};

// supported machines
enum e_machine {
   EM_NONE = 0,
   EM_M32,
   EM_SPARC,
   EM_386,
   EM_68K,
   EM_88K,
   EM_860,
   EM_MIPS,
   EM_AMD86 = 62,
   EM_UK = 101          // used for unknown, invalid, or not implemented options
};

// linking machine enum to printable strings
const char * const machines[] = {
   [EM_NONE] = "No machine",
   [EM_M32] = "AT&T WE 32100",
   [EM_SPARC] = "SPARC",
   [EM_386] = "Intel 80386",
   [EM_68K] = "Motorola 68000",
   [EM_88K] = "Motorola 88000",
   [EM_860] = "Intel 80860",
   [EM_MIPS] = "Mips RS3000",
   [EM_AMD86] = "Advanced Micro Devices X86-64",
   [EM_UK] = "<unknown>"
};

// supported versions
enum e_version {
   EV_NONE = 0,
   EV_CURRENT,
   EV_UK                // used for unknown, invalid, or not implemented options
};

// linking version enum to print strings
const char * const versions[] = {
   [EV_NONE] = "none",
   [EV_CURRENT] = "1 (current)",
   [EV_UK] = "<unknown>"
};

// supported classes
enum ei_class {
   ELFCLASSNONE = 0,
   ELFCLASS32,
   ELFCLASS64,
   ELFCLASSUK          // used for unknown, invalid, or not implemented options
};

// linking class enum to print string
const char * const classes[] = {
   [ELFCLASSNONE] = "none",
   [ELFCLASS32] = "ELF32",
   [ELFCLASS64] = "ELF64",
   [ELFCLASSUK] = "<unknown>"
};

// https://refspecs.linuxfoundation.org/elf/gabi4+/ch4.eheader.html
// supported OSABI
enum ei_OSABI {
   ELFOSABI_NONE = 0,
   ELFOSABI_HPUX,
   ELFOSABI_NETBSD,
   ELFOSABI_LINUX,
   ELFOSABI_SOLARIS,
   ELFOSABI_AIX,
   ELFOSABI_FREEBSD,
   ELFOSABI_TRU64,
   ELFOSABI_ARCH_SPEC = 64,
   ELFOSABI_UK = 8    // used for unknown, invalid, or not implemented options
};

// linking OSABIs enum to print strings
const char* const OSABIs[] = {
   [ELFOSABI_NONE] = "UNIX - System V",
   [ELFOSABI_HPUX] = "Hewlett-Packard HP-UX",
   [ELFOSABI_NETBSD] = "NetBSD",
   [ELFOSABI_LINUX] = "Linux",
   [ELFOSABI_SOLARIS] = "Sun Solaris",
   [ELFOSABI_AIX] = "AIX",
   [ELFOSABI_FREEBSD] = "FreeBSD",
   [ELFOSABI_TRU64] = "Compaq TRU64 UNiX",
   [ELFOSABI_ARCH_SPEC] = "Architecture-specific value range",
   [ELFOSABI_UK] = "<unknown>"
};

// supported data encodings
enum ei_data {
   ELFDATANONE = 0,
   ELFDATA2LSB,
   ELFDATA2MSB,
   ELFDATAUK       // used for unknown, invalid, or not implemented options
};

// linking data enum to print strings
const char* const datas[] = {
   [ELFDATANONE] = "none",
   [ELFDATA2LSB] = "2's compliment, little endian",
   [ELFDATA2MSB] = "2's compliment, big endian",
   [ELFDATAUK] = "<unknown>"
};

// error out of program
int error() {
   fprintf(stderr, "%s: error reading file\n", PROGRAM);
   exit(EXIT_FAILURE);
}

//https://cs-fundamentals.com/tech-interview/c/c-program-to-check-little-and-big-endian-architecture
// test machines endiannes
// returns 1 if LE, 0 if BE
int check_arch_Lendian(){
   unsigned int x = 1;
   char *c = (char*) &x;
   return (int)*c;
}

// read 16 bit value
// respects endiannes
uint16_t read16(FILE* fp) {
   uint16_t readBytes;
   if(fread(&readBytes, 2, 1, fp) != 1) error();
   return readBytes; 
}

// read 32 bit value
// respects endiannes
uint32_t read32(FILE* fp) {
   uint32_t readBytes;
   if(fread(&readBytes, 4, 1, fp) != 1) error();
   return readBytes; 

}

// read 64 bit value
// respects endiannes
uint64_t read64(FILE* fp) {
   uint64_t readBytes;
   if(fread(&readBytes, 8, 1, fp) != 1) error();
      return readBytes;
}

// https://www.csharp-examples.net/reverse-bytes/
// swap endiannes of 16 bit value
uint16_t reverseBytes16(uint16_t bytes) {
   return (uint16_t)((bytes & 0xFFU) << 8 | (bytes & 0xFF00U) >> 8);
}

// https://www.csharp-examples.net/reverse-bytes/
// swap endiannes of 32 bit value
uint32_t reverseBytes32(uint32_t bytes) {
  return (uint32_t)((bytes & 0x000000FFU) << 24 | (bytes & 0x0000FF00U) << 8 |
         (bytes & 0x00FF0000U) >> 8 | (bytes & 0xFF000000U) >> 24);
}

// https://www.csharp-examples.net/reverse-bytes/
// swap endiannes of 64 bit value
uint64_t reverseBytes64(uint64_t bytes) {
  return (uint64_t)((bytes & 0x00000000000000FFUL) << 56 | (bytes & 0x000000000000FF00UL) << 40 |
         (bytes & 0x0000000000FF0000UL) << 24 | (bytes & 0x00000000FF000000UL) << 8 |
         (bytes & 0x000000FF00000000UL) >> 8 | (bytes & 0x0000FF0000000000UL) >> 24 |
         (bytes & 0x00FF000000000000UL) >> 40 | (bytes & 0xFF00000000000000UL) >> 56);
}

// swap endiannes of header struct (only values that need to be swapped)
void reverseHeaderBytes(struct Header* header) {
   header->type = reverseBytes16(header->type);
   header->machine = reverseBytes16(header->machine);
   header->objVersion = reverseBytes32(header->objVersion);

   // check for 32 bit or 64 bit ELF
   switch(header->fileClass) {
      // if 32 bit, reverse 32 bits
      case 1:
         header->entry = reverseBytes32(header->entry);
         header->pheoff = reverseBytes32(header->pheoff);
         header->shoff = reverseBytes32(header->shoff);
         break;
      // if 64 bits, reverse 64 bits
      case 2:
         header->entry = reverseBytes64(header->entry);
         header->pheoff = reverseBytes64(header->pheoff);
         header->shoff = reverseBytes64(header->shoff);
         break;
   }
   header->flags = reverseBytes32(header->flags);
   header->ehsize = reverseBytes16(header->ehsize);
   header->phentsize = reverseBytes16(header->phentsize);
   header->phnum = reverseBytes16(header->phnum);
   header->shentsize = reverseBytes16(header->shentsize);
   header->shnum = reverseBytes16(header->shnum);
   header->shstrndx = reverseBytes16(header->shstrndx);

}

// scans file's ELF header and stores information in header struct
void scan_header(FILE* fp, struct Header* header) {

   // determine endiannes of machine
   // 0 if BE, 1 if LE
   int endiannes = check_arch_Lendian();

   // sanity check for opened file
   if(fp == NULL) {
      fprintf(stderr, "%s: failed to open file\n", PROGRAM);
   }

   // read 16 bytes into magic array
   if(fread(header->magic, 1, 16, fp) != 16) error();

   // setting fields that rely on magic array
   header->fileClass = header->magic[4];
   // if fileClass is out of implemented range, set to ELFCLASSUK
   if(header->fileClass < 0 || header->fileClass > 2) {
      header->fileClass = ELFCLASSUK;
   }

   header->data = header->magic[5];
   // if data out of implemented range, set to ELFDATAUK
   if(header->data < 0 || header->data > 2) {
      header->data = ELFDATAUK;
   }

   header->version = header->magic[6];
   // if version out of implemented range, set to EV_UK
   if (header->version < 0 || header->version > 1) {
      header->version = EV_UK;
   }

   header->OS = header->magic[7];
   // if OS out of implemented range, set to ELFOSABI_UK
   if((header->OS < 0 || header->OS > 7) && header->OS != 64) {
      header->OS = ELFOSABI_UK;
   }

   header->ABIVersion = header->magic[8];

   // read type   
   header->type = read16(fp);

   // read machine architecture type
   header->machine = read16(fp); 

   // read object version
   header->objVersion = read32(fp);

   // determine if 32 bit or 64 bit ELF file, read correct amount of bytes
   switch (header->fileClass) {
      // if 2, do 32bit reads
      case 2:
         header->entry = read64(fp);
         header->pheoff = read64(fp);
         header->shoff = read64(fp);
         break;
      // default to 32 if 1 or out of range
      default:
         header->entry = read32(fp);
         header->pheoff = read32(fp);
         header->shoff = read32(fp);
         break;
   }

   // read flags
   header->flags = read32(fp);

   // read elf header sizeof
   header->ehsize = read16(fp);

   // read header table entry size
   header->phentsize = read16(fp);

   // read number of entries in header table
   header->phnum = read16(fp);

   // read section header size
   header->shentsize = read16(fp);

   // read number of entries in section header table
   header->shnum = read16(fp);

   // read section header table index
   header->shstrndx = read16(fp);

   // if machine is BE but ELF file is LE, swap header struct endiannes
   if(endiannes == 0 && header->data == 1) {
      reverseHeaderBytes(header);
   } else if (endiannes == 1 && header->data == 2) {
      // else if machine is LE but ELF file is BE, swap header struct endiannes
      reverseHeaderBytes(header);
   }

   // if type index is out of implemented range, set to ET_NI
   if((header->type < 0 || header->type > 4) && (header->type != ET_LOPROC && header->type != ET_HIPROC)) {
      header->type = (uint16_t)ET_UK;
   }

   // if machine index is out of implemented range, set to  EM_NI
   if((header->machine < 0 || header->machine > 7) && header->machine != 62) {
      header->machine = (uint16_t)EM_UK;
   }


}

// prints header information
// @Todo add values after <unknown>
void print_header(struct Header header) {

   printf("ELF Header:\n");

   // "magic" array
   printf("  Magic: ");
   for(int i = 0; i < 16; i++)
   {
      printf("%02x ", header.magic[i]);
   }
   printf("\n");

   printf("  Class:                             %s\n", classes[header.fileClass]);
   printf("  Data:                              %s\n", datas[header.data]);
   printf("  Version:                           %s\n", versions[header.version]);
   printf("  OS/ABI:                            %s\n", OSABIs[header.OS]);
   printf("  ABI Version:                       %u\n", header.ABIVersion);
   printf("  Type:                              %s\n", types[header.type]);
   printf("  Machine:                           %s\n", machines[header.machine]);
   printf("  Version:                           %s%x\n", "0x", header.objVersion);
   printf("  Entry point address:               %s%lx\n", "0x", header.entry);
   printf("  Start of program headers:          %lu (bytes into file)\n", header.pheoff);
   printf("  Start of section headers:          %lu (bytes into file)\n", header.shoff);
   printf("  Flags:                             %s%x\n", "0x",header.flags);
   printf("  Size of this header:               %u (bytes)\n", header.ehsize);
   printf("  Size of program headers:           %u (bytes)\n", header.phentsize);
   printf("  Number of program headers:         %u\n", header.phnum);
   printf("  Size of section headers:           %u (bytes)\n", header.shentsize);
   printf("  Number of section headers:         %u\n", header.shnum);
   printf("  Section header string table index: %u\n", header.shstrndx);

}
