/////////////////////////////////////////////////////////////////////////////////
//           University of Hawaii, College of Engineering
/// @brief   Lab05a - readelf - EE 491 - Spr 2022
///
/// @file    scan-header.h
/// @version 1.0 
///
/// readelf - Scans ELF file's ELF header and prints header information 
///
/// @author Jake Imanaka   <jimanaka@hawaii.edu>
/// @date   20_03_2022
///
///////////////////////////////////////////////////////////////////////////////

#ifndef SCAN_HEADERS_H
#define SCAN_HEADERS_H

#include <stdio.h>
#include <stdbool.h>
#include "readelf.h"

// scans ELF header and stores header info in header struct
void scan_header(FILE* fp, struct Header* header);

// prints header information
void print_header(struct Header header);

#endif
