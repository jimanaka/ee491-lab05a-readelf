/////////////////////////////////////////////////////////////////////////////////
//           University of Hawaii, College of Engineering
/// @brief   Lab05a - readelf - EE 491 - Spr 2022
///
/// @file    readelf.h
/// @version 1.0 
///
/// readelf - Scans ELF file's ELF header and prints header information 
///
/// @author Jake Imanaka   <jimanaka@hawaii.edu>
/// @date   20_03_2022
///
///////////////////////////////////////////////////////////////////////////////

#ifndef READELF_H
#define READELF_H

#include <stdint.h>

#define PROGRAM "readelf"

//https://laulima.hawaii.edu/access/content/attachment/MAN.87623.202230/Tests%20_%20Quizzes/b3de233d-a367-4d44-85f3-51181b1a5ff9/elf.pdf
//https://laulima.hawaii.edu/access/content/attachment/MAN.87623.202230/Tests%20_%20Quizzes/ca0257af-5237-4c2b-a416-843d894b5d33/ELF_Format.pdf
// struct to represent ELF header
struct Header {
   /////// 16 byte
   unsigned char magic[16];
   unsigned char fileClass;         // none, 32, 64 bit  | magic[4]
   unsigned char data;              // little/big endian | magic[5] 
   unsigned char version;           // ELF version       | magic[6]
   unsigned char OS;                // OS                | magic[7]
   unsigned char ABIVersion;        // ABI verison       | magic[8]
                                    // padding           | magic[9-15]
   uint16_t      type;              // object file type
   uint16_t      machine;           // required architecture
   uint32_t      objVersion;        // object file version
   uint64_t      entry;             // virtual address entry point
   uint64_t      pheoff;            // header table offset
   uint64_t      shoff;             // section header table's file offset
   uint32_t      flags;             // processor-specific flags
   uint16_t      ehsize;            // ELF header's size in bytes
   uint16_t      phentsize;         // size in bytes of one entry in file's header table
   uint16_t      phnum;             // number of entries in the program header table
   uint16_t      shentsize;         // section header's size in bytes
   uint16_t      shnum;             // number of entries in section header table
   uint16_t      shstrndx;          // section header table index
};

#endif
