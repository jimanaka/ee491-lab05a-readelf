###############################################################################
###          University of Hawaii, College of Engineering
### @brief   Lab 05a - readelf - EE 491 - Spr 2022
###
### @file    Makefile
### @version 1.0 
###
### readelf - Scans ELF file's ELF header and prints header information 
###
### @author  Jake Imanaka	<jimanaka@hawaii.edu>
### @date    20_03_2022
###
###############################################################################

CC			= gcc
CFLAGS	= -g -Wall -I ./headers/
TARGET	= readelf 
OBJFILES	= readelf.o scan-header.o
HEADERS 	= headers/scan-header.h 

all: $(TARGET)

$(TARGET): $(OBJFILES)
	$(CC) -o $(TARGET) $(OBJFILES)

readelf.o: src/readelf.c $(HEADERS)
	$(CC) $(CFLAGS) -c -o readelf.o src/readelf.c

scan-header.o: src/scan-header.c $(HEADERS)
	$(CC) $(CFLAGS) -c -o scan-header.o src/scan-header.c

test:
	@echo "testing ./readelf -h 64 bit little endian ..."
	./readelf -h test-files/test64
	@echo "#################################################"
	@echo "testing ./readelf -h 32 bit little endian ..."
	./readelf -h test-files/test32
	@echo "#################################################"
	@echo "testing ./readelf -h 64 bit big endian ..."
	./readelf -h test-files/bigtest64
	@echo "#################################################"
	@echo "testing ./readelf -h 32 bit big endian ..."
	./readelf -h test-files/bigtest32
	@echo "#################################################"
	@echo "testing ./readelf -h invalid 64 bit little endian"
	./readelf -h test-files/invalid-test64
	@echo "#################################################"
	@echo "testing complete"

clean:
	rm -f $(TARGET) *.o

